﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Plansza
    {
       private Pole[,] tablicaPol = new Pole[12,12];
       
        public Plansza()
        {
            for(int i = 0; i < 12; i++)
            {
                for(int j = 0; j < 12; j++)
                {
                    tablicaPol[i,j] = new Pole(false);
                }
            }
        }
        public Boolean getStanPola(int x, int y)
        {
                return this.tablicaPol[x, y].getAktywny();       
        }
        public void narysujPlansze()
        {
            for(int i = 1; i < 11; i++)
            {
                for(int j = 1 ; j < 11; j++)
                {
                    tablicaPol[i, j].narysujPole();
                }
                Console.WriteLine("");
            }
        }
        public void ustawPoleNaPlanszy(int x, int y, Boolean stan)
        {
            this.tablicaPol[x, y].setAktywny(stan);
        }
        public Boolean ustalStanPola(int x, int y)
        {
            int licznikPolZywych = 0;

            for (int i = x - 1; i < x + 2; i++)
            {
                for (int j = y - 1; j < y + 2; j++)
                {
                    if (i == x && j == y) { }
                    else 
                    if (this.tablicaPol[i, j].getAktywny())
                    {
                        licznikPolZywych++;
                    }
                }
            }
            if(this.tablicaPol[x,y].getAktywny())
            {
                if (licznikPolZywych == 2 || licznikPolZywych == 3)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (licznikPolZywych == 3)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
