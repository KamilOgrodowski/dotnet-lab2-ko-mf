﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class GameOfLife
    {
        private Plansza obecnaGeneracja;
        private Plansza nastepnaGeneracja;

        private List<Punkt> listaPunktowZywych;
             
        private void ustalPoczatkowePunktyZywe()
        {
            int x,y;
            Console.WriteLine("Podaj liczbe poczatkowych punktow zywych");
            int N = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Podaj wspolrzedne punktow");
            for (int i = 0; i < N; i++)
            {
                string wspolrzedne = Console.ReadLine();
                string[] dane = wspolrzedne.Split(' ');
                x = Convert.ToInt32(dane[0]);
                y = Convert.ToInt32(dane[1]);
                if (x > 0 && y > 0 && y < 11 && x < 11)
                {
                    obecnaGeneracja.ustawPoleNaPlanszy(x, y, true);
                    this.listaPunktowZywych.Add(new Punkt(x, y));
                }
                else
                {
                    i--;
                    Console.WriteLine("Podales niewlasciwe wspolrzedne punktu poczatkowego ! x i y musza zawierac sie w przedziale od 1 do 10 ");
                }
            }
        }
        public GameOfLife()
        {
            this.obecnaGeneracja = new Plansza();
            this.nastepnaGeneracja = new Plansza();
            this.listaPunktowZywych = new List<Punkt>();
            ustalPoczatkowePunktyZywe();
            Console.Clear();
        }
        public void run()
        {
            Boolean result;
            while(true)
            { 
                for (int i = 1 ; i < 11; i++)
             {
                   for (int j = 1; j < 11 ;j++ )
                   {
                    result = this.obecnaGeneracja.ustalStanPola(i, j);
                    this.nastepnaGeneracja.ustawPoleNaPlanszy(i, j, result);
                 }
             }
                for (int i = 0; i < this.listaPunktowZywych.Count; i++)
                {
                 result = this.obecnaGeneracja.ustalStanPola(this.listaPunktowZywych[i].getX(), this.listaPunktowZywych[i].getY());
                 this.nastepnaGeneracja.ustawPoleNaPlanszy(this.listaPunktowZywych[i].getX(), this.listaPunktowZywych[i].getY(), result);
                }
                obecnaGeneracja.narysujPlansze();
                System.Threading.Thread.Sleep(500);
                Console.Clear();
                nastepnaGeneracja.narysujPlansze();
                aktualizujListePunktowZywych();
                this.obecnaGeneracja = this.nastepnaGeneracja;
                this.nastepnaGeneracja = new Plansza();
                Console.Clear();
            }
        }
        private void aktualizujListePunktowZywych()
        {
            this.listaPunktowZywych.Clear();
            for (int i = 1; i < 11; i++)
            {
                for (int j = 1; j < 11; j++)
                {
                    if (this.nastepnaGeneracja.getStanPola(i, j))
                    {
                        this.listaPunktowZywych.Add(new Punkt(i, j));
                    }
                }
            }
        }
    }
}
